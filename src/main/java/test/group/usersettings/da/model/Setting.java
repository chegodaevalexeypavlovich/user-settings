package test.group.usersettings.da.model;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "SETTINGS")
public class Setting {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "value")
    private String value;
    @ManyToOne(fetch=LAZY)
    private User user;

    public Setting() {
    }

    public Setting(Long id, String name, String value, User user) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.user = user;
    }




    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
