package test.group.usersettings.da.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import test.group.usersettings.da.model.Setting;

import java.util.List;

public interface SettingsRepository extends JpaRepository<Setting, Long> {

    List<Setting> findByName(String Name);

    @Query("SELECT s FROM Setting s WHERE  s.name=?1 AND s.user.id = ?2 ")
    Setting findBySettingNameAndUserId(String name, Long id);

    Setting findById(long id);

    @Query("SELECT s FROM Setting s WHERE s.user.id = ?1 ")
    List<Setting> findByUserLogin(Long login);

    @Modifying
    @Transactional
    @Query("UPDATE Setting s SET  s.value=?2 WHERE s.id = ?1")
    void updateAddress(long id, String value);

}