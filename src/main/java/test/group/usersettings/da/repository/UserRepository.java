package test.group.usersettings.da.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.group.usersettings.da.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
