package test.group.usersettings.endpoint.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import test.group.usersettings.bs.monitoring.MonitoringService;
import test.group.usersettings.bs.user.UserService;
import test.group.usersettings.endpoint.dto.ErrorDto;
import test.group.usersettings.endpoint.dto.UserDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    @ResponseBody
    public UserDto createSettings(@RequestBody UserDto user) {
        return userService.createUser(user);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll() {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return new ResponseEntity<>(
                new ErrorDto(dateTime.format(formatter), "Произошла ошибка, обратитесь к администратору системы"), new HttpHeaders(), HttpStatus.OK);
    }
}
