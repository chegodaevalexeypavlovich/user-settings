package test.group.usersettings.endpoint.endpoint;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import test.group.usersettings.bs.monitoring.MonitoringService;
import test.group.usersettings.bs.settings.SettingsService;
import test.group.usersettings.endpoint.dto.ErrorDto;
import test.group.usersettings.endpoint.dto.OperationStatusDto;
import test.group.usersettings.endpoint.dto.SettingsDto;


@Controller
@RequestMapping("/settings")
public class SettingsController {
    private final SettingsService settingsService;
    private final MonitoringService monitoringService;

    public static final OperationStatusDto SUCCESS_STATUS = new OperationStatusDto(true, "OK");
    public static final OperationStatusDto FAIL_STATUS = new OperationStatusDto(false, "Ошибка");

    public SettingsController(SettingsService settingsService, MonitoringService monitoringService) {
        this.settingsService = settingsService;
        this.monitoringService = monitoringService;
    }

    @PutMapping("/create")
    @ResponseBody
    public SettingsDto createSettings(@RequestBody SettingsDto setting) {
        return settingsService.createSettings(setting);
    }

    @PostMapping("/update")
    @ResponseBody
    public void updateSettings(@RequestBody SettingsDto setting) {
         settingsService.updateSettings(setting);
    }

    @DeleteMapping("/delete")
    @ResponseBody
    public void deleteSettings(@RequestBody SettingsDto setting) {
        settingsService.deleteSettings(setting);
    }

    /**
     * получить настроку по логину пользователя
     *
     * @return список настроек
     */
    @GetMapping("/get")
    @ResponseBody
    public List<SettingsDto> getSettings(@RequestParam(name = "id") Long id) {
        return settingsService.getSettingsByUserId(id);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll() {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return new ResponseEntity<>(
                new ErrorDto(dateTime.format(formatter), "Произошла ошибка, обратитесь к администратору системы"), new HttpHeaders(), HttpStatus.OK);
    }
}
