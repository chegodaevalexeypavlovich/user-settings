package test.group.usersettings.endpoint.dto;

public class ErrorDto {
    private String ts;
    private String message;

    public ErrorDto() {
    }

    public ErrorDto(String ts, String message) {
        this.ts = ts;
        this.message = message;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
