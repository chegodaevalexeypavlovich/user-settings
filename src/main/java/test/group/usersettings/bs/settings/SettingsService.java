package test.group.usersettings.bs.settings;

import org.springframework.stereotype.Component;
import test.group.usersettings.SettingsMapper;
import test.group.usersettings.bs.BusinessException;
import test.group.usersettings.da.model.Setting;
import test.group.usersettings.da.repository.SettingsRepository;
import test.group.usersettings.endpoint.dto.SettingsDto;

import java.util.List;

@Component
public class SettingsService {
    private final SettingsRepository settingsRepository;
    private final SettingsMapper settingsMapper;

    public SettingsService(SettingsRepository settingsRepository, SettingsMapper settingsMapper) {
        this.settingsRepository = settingsRepository;
        this.settingsMapper = settingsMapper;
    }

    public List<SettingsDto> getSettingsByUserId(Long id) {
        List<Setting> settings = settingsRepository.findByUserLogin(id);
        return settingsMapper.settingsToSettingsDto(settings);
    }

    public SettingsDto createSettings(SettingsDto dto) {
        Setting setting = settingsRepository.findBySettingNameAndUserId(dto.getName(), dto.getUserId());
        if (setting != null) {
            throw new BusinessException("Попытка создать уже существующую настройку", "DUPLICATE_KONF_CREATION_ERROR");
        }
        Setting settings = settingsMapper.settingsDtoToSettings(dto);
        Setting savedSetting = settingsRepository.save(settings);
        return settingsMapper.settingsToSettingsDto(savedSetting);
    }

    public void updateSettings(SettingsDto dto) {
         settingsRepository.updateAddress(dto.getId(), dto.getValue());

    }

    public void deleteSettings(SettingsDto dto) {
        Setting setting = settingsMapper.settingsDtoToSettings(dto);
        settingsRepository.delete(setting);
    }

}
