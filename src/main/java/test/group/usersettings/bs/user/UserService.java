package test.group.usersettings.bs.user;

import org.springframework.stereotype.Component;
import test.group.usersettings.UserMapper;
import test.group.usersettings.da.model.User;
import test.group.usersettings.da.repository.UserRepository;
import test.group.usersettings.endpoint.dto.UserDto;

@Component
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public UserDto createUser(UserDto dto) {
        User user = userMapper.userDtoToUser(dto);
        return userMapper.userToUserDto(userRepository.save(user));
    }
}
