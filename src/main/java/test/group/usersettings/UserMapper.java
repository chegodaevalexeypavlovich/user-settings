package test.group.usersettings;

import org.mapstruct.Mapper;


import test.group.usersettings.da.model.User;
import test.group.usersettings.endpoint.dto.UserDto;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto user);

}
