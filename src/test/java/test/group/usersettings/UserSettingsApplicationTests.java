package test.group.usersettings;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import test.group.usersettings.endpoint.dto.SettingsDto;
import test.group.usersettings.endpoint.dto.UserDto;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * TODO Переписать по нормальному
 */
@SpringBootTest
@AutoConfigureMockMvc
class UserSettingsApplicationTests {
    @Autowired
    private MockMvc mvc;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testProcess() throws Exception {
        String responseBody = mvc.perform(MockMvcRequestBuilders
                .post("/user/create")
                .content(objectMapper.writeValueAsString(new UserDto("login1")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn().getResponse().getContentAsString();
        UserDto userDto = objectMapper.readValue(responseBody, UserDto.class);

        responseBody = mvc.perform(MockMvcRequestBuilders
                .put("/settings/create")
                .content(objectMapper.writeValueAsString(new SettingsDto("name1", "value1", userDto.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn().getResponse().getContentAsString();

        SettingsDto settingsDto = objectMapper.readValue(responseBody, SettingsDto.class);

//попытка создать ту же конфигурацию
        mvc.perform(MockMvcRequestBuilders
                .put("/settings/create")
                .content(objectMapper.writeValueAsString(new SettingsDto("name1", "value1", userDto.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").exists());

        mvc.perform(MockMvcRequestBuilders
                .get("/settings/get")
                .param("id", String.valueOf(userDto.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("[0].id").exists());

        settingsDto.setValue("value2");
        mvc.perform(MockMvcRequestBuilders
                .post("/settings/update")
                .content(objectMapper.writeValueAsString(settingsDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());

        mvc.perform(MockMvcRequestBuilders
                .get("/settings/get")
                .param("id", String.valueOf(userDto.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("[0].value").value(Is.is(settingsDto.getValue())));

        mvc.perform(MockMvcRequestBuilders
                .delete("/settings/delete")
                .content(objectMapper.writeValueAsString(settingsDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());

        mvc.perform(MockMvcRequestBuilders
                .get("/settings/get")
                .param("id", String.valueOf(userDto.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().string("[]"));

    }

}
